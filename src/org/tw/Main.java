package org.tw;

public class Main {
    public static void main(String[] args) {
        //call by value
        int x = 10;
        modify(x);
        System.out.println("inside main " + x);

        //call by reference
        Rectangle r1 =  new Rectangle();
        r1.length=10;
        r1.breadth=20;

        modify(r1);
        System.out.println("r1 inside main " +r1);
    }

    private static void modify(Rectangle r2) {
        r2.length=30;
        r2.breadth=40;
        System.out.println("r2 inside modify "+r2);
    }

    private static void modify(int x) {
        x = 20;
        System.out.println("inside modify " + x);
    }
}
